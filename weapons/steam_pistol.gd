extends Weapon
# See Weapon class for some generic members/functions

const BULLET = preload("res://weapons/projectiles/bullet.tscn")

@onready var shot_cooldown = $ShotCooldown

var target : Vector2

func _ready():
	spread = 10.0

func attack():
	var bullet = BULLET.instantiate()
	
	# set bullet damage, type, speed and duration (0 or "" for defaults)
	bullet.setup(20, rune, 600.0, 2.5, 800.0)
	
	bullet.set_global_position(get_global_position())
	target = (get_global_mouse_position() - get_global_position())
	
	var calculated_target = calculate_target(target)
	bullet.set_direction(calculated_target)
	bullet.look_at(get_global_position() + calculated_target)
	
	get_parent().get_parent().get_parent().add_child(bullet)
	
	shot_cooldown.start()

func can_attack() -> bool:
	if shot_cooldown.is_stopped():
		return true
	else:
		return false

func check_upgrade() -> void:
	pass
