# Virtual base class for all weapons.
class_name Weapon
extends Node2D

var spread : float

var rune : Array[String]

# Virtual function. Weapon override for custom attacks.
func attack() -> void:
	pass

# Virtual function. Must be overriden in order to make a weapon attack
func can_attack() -> bool:
	return false

# add randomized spread to attack
func calculate_target(target : Vector2):
	var perpendicular = Vector2(target.y, -target.x).normalized()
	return (target + (randf_range(-1.0, 1.0) * perpendicular * spread)).normalized()

# set function for rune type
func add_rune(rune_input):
	if not rune.has(rune_input):
		rune.push_back(rune_input)
		#check_upgrade()

# Virtual function. Must be overriden to upgrade weapon with certain runes.
func check_upgrade() -> void:
	pass
