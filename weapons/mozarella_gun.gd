extends Weapon
# See Weapon class for some generic members/functions

const MOZARELLA = preload("res://weapons/projectiles/mozarella.tscn")

@onready var shot_cooldown = $ShotCooldown
@onready var audio_stream_player = $AudioStreamPlayer
const BLOP = preload("res://assets/audio/sfx/momohut/blop.wav")
const BLOP_1 = preload("res://assets/audio/sfx/momohut/blop1.wav")
const BLOP_2 = preload("res://assets/audio/sfx/momohut/blop2.wav")

var target : Vector2

@onready var sprite_2d = $Sprite2D

func _ready():
	spread = 25.0

func attack():
	var bullet = MOZARELLA.instantiate()
	
	# set bullet damage, type, speed and duration (0 or "" for defaults)
	bullet.setup(10, 0, 0, 0, "player")
	
	bullet.set_global_position(get_global_position())
	target = (get_global_mouse_position() - get_global_position())
	
	var calculated_target = calculate_target(target)
	bullet.set_direction(calculated_target)
	bullet.look_at(get_global_position() + calculated_target)
	bullet.global_position += calculated_target.normalized() * 20
	
	get_tree().current_scene.add_child(bullet)
	
	shot_cooldown.start()
	
	play_blop()
	
	for child in get_tree().current_scene.get_children():
		if child.has_method("add_trauma"):
			child.add_trauma(0.3)

func can_attack() -> bool:
	if shot_cooldown.is_stopped():
		return true
	else:
		return false

func play_blop():
	var selection = randi_range(1, 3)
	match selection:
		1:
			audio_stream_player.set_stream(BLOP)
		2:
			audio_stream_player.set_stream(BLOP_1)
		3:
			audio_stream_player.set_stream(BLOP_2)
	
	audio_stream_player.play()

func flip_h():
	if (get_global_mouse_position() - global_position < Vector2.ZERO) and not sprite_2d.is_flipped_v():
		sprite_2d.set_flip_v(true)
		sprite_2d.set_offset(Vector2(20, -10))
	if (get_global_mouse_position() - global_position >= Vector2.ZERO) and sprite_2d.is_flipped_v():
		sprite_2d.set_flip_v(false)
		sprite_2d.set_offset(Vector2(20, 10))
