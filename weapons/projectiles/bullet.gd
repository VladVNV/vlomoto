extends Node2D

var direction : Vector2

var damage := 10
var type := ""
var fly_speed := 200.0
var fly_duration := 1.5
var knockback_power := 300.0

var source := "player"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if fly_duration <= 0.0:
		queue_free()
	
	position += direction * fly_speed * delta
	
	fly_duration -= delta

func set_direction(dir):
	direction = dir

func _on_hit_box_body_entered(body):
	if body.is_in_group("enemy") and source == "player":
		body.hit(self)
		queue_free()
	elif body.is_in_group("player") and source == "enemy":
		body.hit(self)
		queue_free()
	if body.is_in_group("terrain"):
		queue_free()

func setup(damage_input : int, type_input : Array[String], speed_input : float, duration_input : float, \
			knockback_power_input : float, source_input : String):
	damage = damage_input if damage_input > 0 else damage
	
	# TODO: fix type handling to mix damage types somehow (currently uses first type)
	type = type_input[0] if type_input else type
	
	fly_speed = speed_input if speed_input > 0 else fly_speed
	fly_duration = duration_input if duration_input > 0 else fly_duration
	knockback_power = knockback_power_input if knockback_power_input > 0 else knockback_power
	source = source_input if source_input != "" else source
