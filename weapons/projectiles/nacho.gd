extends Node2D

var direction : Vector2

var damage := 10
var type := ""
var fly_speed := 200.0
var fly_duration := 1.2
var knockback_power := 150.0

var source := "player"
@onready var hit_box = $HitBox

const SPLAT = preload("res://weapons/effects/splat.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if fly_duration <= 0.0:
		summon_splat()
	
	position += direction * fly_speed * delta
	
	fly_duration -= delta
	
	check_for_tree()

func set_direction(dir):
	direction = dir

func _on_hit_box_body_entered(body):
	if body.is_in_group("enemy") and source == "player":
		body.hit(self)
		delete_shot()
	elif body.is_in_group("player") and source == "enemy":
		body.hit(self)
		delete_shot()
	if body.is_in_group("terrain"):
		check_for_tree()

func setup(damage_input : int, speed_input : float, duration_input : float, \
			knockback_power_input : float, source_input : String):
	damage = damage_input if damage_input > 0 else damage
	
	fly_speed = speed_input if speed_input > 0 else fly_speed
	fly_duration = duration_input if duration_input > 0 else fly_duration
	knockback_power = knockback_power_input if knockback_power_input > 0 else knockback_power
	source = source_input if source_input != "" else source

func summon_splat():
	var splat = SPLAT.instantiate()
	
	splat.set_global_position(get_global_position())
	
	get_tree().current_scene.call_deferred("add_child", splat)
	
	queue_free()

func check_for_tree():
	var tile_pos = $".."/TileMap.local_to_map(global_position)
	var tile = $".."/TileMap.get_cell_tile_data(2, tile_pos)
	if tile != null:
		if tile.get_custom_data("isObstacle"):
			delete_shot()

func delete_shot():
	summon_splat()
