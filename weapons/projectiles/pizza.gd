extends Node2D

var direction : Vector2

var damage := 10
var type := ""
var fly_speed := 140.0
var fly_duration := 1.5
var knockback_power := 50.0
var rotation_power := 10

var source := "player"

@onready var swoosh = $Swoosh
@onready var swish = $Swish
@onready var swish_timer = $SwishTimer
@onready var hit = $Hit

# Called when the node enters the scene tree for the first time.
func _ready():
	if randi_range(0, 1) == 1:
		rotation_power *= -1
	swoosh.play()
	swish_timer.start()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if fly_duration <= 0.0:
		fly_reverse(delta)
		return
	
	position += direction * fly_speed * delta
	
	fly_duration -= delta
	
	rotation += delta * rotation_power

func fly_reverse(delta):
	var target_position
	
	for child in get_tree().current_scene.get_children():
		if child.has_method("player_position"):
			target_position = child.player_position()
	
	target_position = (target_position - global_position).normalized()
	position += target_position * fly_speed * delta
	
	rotation -= delta * rotation_power

func set_direction(dir):
	direction = dir

func _on_hit_box_body_entered(body):
	if body.is_in_group("enemy") and source == "player":
		body.hit(self)
		hit.play()
	elif body.is_in_group("player") and source == "player" and fly_duration <= 0.0:
		delete_shot()

func setup(damage_input : int, speed_input : float, duration_input : float, \
			knockback_power_input : float, source_input : String):
	damage = damage_input if damage_input > 0 else damage
	
	fly_speed = speed_input if speed_input > 0 else fly_speed
	fly_duration = duration_input if duration_input > 0 else fly_duration
	knockback_power = knockback_power_input if knockback_power_input > 0 else knockback_power
	source = source_input if source_input != "" else source

func delete_shot():
	queue_free()

func _on_swish_timer_timeout():
	swish.play()
	swish_timer.start()
