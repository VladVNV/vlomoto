extends Weapon
# See Weapon class for some generic members/functions

const SWISS = preload("res://weapons/projectiles/swiss.tscn")

@onready var shot_cooldown = $ShotCooldown
@onready var audio_stream_player = $AudioStreamPlayer
const TICK = preload("res://assets/audio/sfx/momohut/tick.wav")
const TICK_1 = preload("res://assets/audio/sfx/momohut/tick1.wav")
const TICK_2 = preload("res://assets/audio/sfx/momohut/tick2.wav")
const TICK_3 = preload("res://assets/audio/sfx/momohut/tick3.wav")

@onready var sprite_2d = $Sprite2D

var target : Vector2

func _ready():
	spread = 25.0

func attack():
	if randi_range(0, 9) > 6:
		shot_cooldown.start()
		return
	
	var bullet = SWISS.instantiate()
	
	# set bullet damage, type, speed and duration (0 or "" for defaults)
	bullet.setup(10, 0, 0, 0, "player")
	
	bullet.set_global_position(get_global_position())
	target = (get_global_mouse_position() - get_global_position())
	
	var calculated_target = calculate_target(target)
	bullet.set_direction(calculated_target)
	bullet.look_at(get_global_position() + calculated_target)
	bullet.global_position += calculated_target.normalized() * 20
	
	get_tree().current_scene.add_child(bullet)
	
	shot_cooldown.start()
	
	play_tick()
	
	for child in get_tree().current_scene.get_children():
		if child.has_method("add_trauma"):
			if child.trauma < 0.3:
				child.add_trauma(0.15)
		if child.has_method("use_ammo"):
			child.use_ammo(1)

func can_attack() -> bool:
	if shot_cooldown.is_stopped():
		return true
	else:
		return false

func play_tick():
	var selection = randi_range(1, 4)
	match selection:
		1:
			audio_stream_player.set_stream(TICK)
		2:
			audio_stream_player.set_stream(TICK_1)
		3:
			audio_stream_player.set_stream(TICK_2)
		4:
			audio_stream_player.set_stream(TICK_3)
	
	audio_stream_player.play()

func flip_h():
	if (get_global_mouse_position() - global_position < Vector2.ZERO) and not sprite_2d.is_flipped_v():
		sprite_2d.set_flip_v(true)
		sprite_2d.set_offset(Vector2(20, -10))
	if (get_global_mouse_position() - global_position >= Vector2.ZERO) and sprite_2d.is_flipped_v():
		sprite_2d.set_flip_v(false)
		sprite_2d.set_offset(Vector2(20, 10))
