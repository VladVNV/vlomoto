extends Weapon
# See Weapon class for some generic members/functions

const BULLET = preload("res://weapons/projectiles/bullet.tscn")

@onready var shot_cooldown = $ShotCooldown

var target : Vector2

func _ready():
	spread = 25.0

func attack():
	var bullet = BULLET.instantiate()
	
	# set bullet damage, type, speed and duration (0 or "" for defaults)
	bullet.setup(10, rune, 0, 0, 0, "player")
	
	bullet.set_global_position(get_global_position())
	target = (get_global_mouse_position() - get_global_position())
	
	var calculated_target = calculate_target(target)
	bullet.set_direction(calculated_target)
	bullet.look_at(get_global_position() + calculated_target)
	bullet.global_position += calculated_target.normalized() * 20
	
	get_tree().current_scene.add_child(bullet)
	
	shot_cooldown.start()
	
	for child in get_tree().current_scene.get_children():
		if child.has_method("add_trauma"):
			child.add_trauma(0.3)

func can_attack() -> bool:
	if shot_cooldown.is_stopped():
		return true
	else:
		return false

func check_upgrade() -> void:
	if rune.has("fire") and rune.has("water"):
		get_parent().change_weapon("SteamPistol")
