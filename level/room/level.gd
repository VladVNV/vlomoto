extends Node

const base = "res://level/room/"

func load(filename):
	var root = Node2D.new()
	var f = FileAccess.open(filename, FileAccess.READ)
	var json = JSON.parse_string(f.get_as_text())
	var rooms = json["rooms"]
	var x = 0
	var y = 0
	for key in rooms:
		var file = base + key + ".tscn"
		var resource: PackedScene = load(file)
		var inst = resource.instantiate()
		var tilemap: TileMap = inst
		var rect = tilemap.get_used_rect()
		inst.position.x += x - rect.position.x * tilemap.tile_set.tile_size.x
		inst.position.y += y - rect.position.y * tilemap.tile_set.tile_size.y
		x += rect.size.x * tilemap.tile_set.tile_size.x
		root.add_child(inst)
	return root
