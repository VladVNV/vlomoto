extends Enemy
class_name Ghost

@onready var damage_text_marker = $DamageTextMarker
@onready var navigation_agent_2d = $NavigationAgent2D
@onready var trigger_navigation = $TriggerNavigation
@onready var sprite_2d = $Sprite2D
@onready var attack_timer = $AttackTimer
@onready var detection_range = $DetectionRange
@onready var animation_player = $AnimationPlayer

func _ready():
	speed = 40

func hit(source):
	# pass damage source to get info like damage value and type
	health -= source.damage
	if health <= 0:
		is_dying = true
		remove_from_group("enemy")
	print_damage(source.damage, source.type)
	if source.knockback_power:
		knockback(source.direction, source.knockback_power)
	flash()

func print_damage(damage, type):
	# generate damage text
	var text = floating_text.instantiate()
	text.value = damage
	text.type = type
	text.set_position(damage_text_marker.position)
	add_child(text)

func _on_detection_range_body_entered(body):
	# TODO: should detection range be defined by the room instead of a circle?
	if body.is_in_group("player"):
		player_detected = true
		player_reference = body

func _on_detection_range_body_exited(body):
	if body.is_in_group("player"):
		player_detected = false

func _on_attack_range_body_entered(body):
	if body.is_in_group("player"):
		trigger_attack = true
		cancel_attack = false

func _on_attack_range_body_exited(body):
	if body.is_in_group("player"):
		cancel_attack = true
