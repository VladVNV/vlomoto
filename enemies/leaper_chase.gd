extends EnemyState

var target_direction

func enter(_msg := {}) -> void:
	enemy.trigger_navigation.start()
	calculate_direction()
	#print("enemy chase")
	enemy.animation_player.play("chase")
	pass

func exit() -> void:
	enemy.trigger_navigation.stop()
	enemy.animation_player.stop()
	pass

func physics_update(delta) -> void:
	# var target_direction = (enemy.player_reference.global_position - enemy.global_position).normalized()
	enemy.velocity = lerp(enemy.velocity, target_direction * enemy.speed, enemy.acceleration * delta)
	enemy.move_and_slide()
	
	if enemy.is_dying:
		state_machine.transition_to("Die")
	if enemy.trigger_attack:
		if enemy.is_in_group("leaper"):
			state_machine.transition_to("Attack")
		elif enemy.is_in_group("shooter"):
			state_machine.transition_to("Shoot")
	if not enemy.detection_range.get_overlapping_bodies().has(enemy.player_reference):
		state_machine.transition_to("Patrol")

func _on_trigger_navigation_timeout():
	calculate_direction()

func calculate_direction():
	enemy.navigation_agent_2d.target_position = enemy.player_reference.global_position
	target_direction = (enemy.navigation_agent_2d.get_next_path_position() - enemy.global_position).normalized()
