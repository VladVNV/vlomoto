extends EnemyState

var calculated_target
var initial_direction

var attack_finished := false
var attack_spread := 10.0
var runes : Array[String]

const BULLET = preload("res://weapons/projectiles/bullet.tscn")

func enter(_msg := {}) -> void:
	enemy.trigger_attack = false
	attack_anim()
	#print("enemy shoot")
	pass

func exit() -> void:
	enemy.velocity = Vector2.ZERO
	pass

func physics_update(_delta) -> void:
	
	if enemy.is_dying:
		state_machine.transition_to("Die")
	if attack_finished and not enemy.cancel_attack:
		state_machine.transition_to("Shoot")
		attack_finished = false
	if attack_finished and enemy.cancel_attack:
		state_machine.transition_to("Chase")
		attack_finished = false

func attack_anim():
	var anim_tween = get_tree().create_tween()
	initial_direction = (enemy.player_reference.global_position - enemy.global_position)
	anim_tween.tween_property(enemy.sprite_2d, "scale", Vector2(0.7, 0.7), 0.2).\
	set_trans(Tween.TRANS_LINEAR).set_ease(Tween.EASE_OUT)
	anim_tween.tween_property(enemy.sprite_2d, "scale", Vector2(1, 1), 0.2).\
	set_trans(Tween.TRANS_LINEAR).set_ease(Tween.EASE_OUT)
	await anim_tween.finished
	
	calculated_target = calculate_target(enemy.player_reference.global_position - enemy.global_position, attack_spread)
	var bullet = BULLET.instantiate()
	# set bullet damage, type, speed and duration (0 or "" for defaults)
	bullet.setup(10, runes, 0, 0, 0, "enemy")
	bullet.set_global_position(enemy.global_position)
	bullet.set_direction(calculated_target)
	bullet.look_at(enemy.global_position + calculated_target)
	bullet.global_position += calculated_target.normalized() * 20
	get_tree().current_scene.add_child(bullet)
	
	enemy.attack_timer.start()
	
	anim_tween = get_tree().create_tween()
	anim_tween.tween_property(enemy.sprite_2d, "scale", Vector2(0.7, 0.7), 0.2).\
	set_trans(Tween.TRANS_LINEAR).set_ease(Tween.EASE_OUT)
	anim_tween.tween_property(enemy.sprite_2d, "scale", Vector2(1, 1), 0.2).\
	set_trans(Tween.TRANS_LINEAR).set_ease(Tween.EASE_OUT)

func _on_attack_timer_timeout():
	attack_finished = true

# add randomized spread to attack
func calculate_target(target : Vector2, spread : float):
	var perpendicular = Vector2(target.y, -target.x).normalized()
	return (target + (randf_range(-1.0, 1.0) * perpendicular * spread)).normalized()
