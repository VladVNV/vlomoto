extends EnemyState

func enter(_msg := {}) -> void:
	death_anim()

func physics_update(_delta) -> void:
	pass

func death_anim() -> void:
	var anim_tween = get_tree().create_tween()
	anim_tween.tween_property(enemy, "modulate", Color(1, 1, 1, 0), 1.0)
	
	await anim_tween.finished
	enemy.queue_free()
