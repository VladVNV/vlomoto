class_name Enemy
extends CharacterBody2D

@export_category("movement")
@export var speed := 75
@export var acceleration := 18
@export var friction := 13
@export var jump_power := 300

@export_category("stats")
@export var health := 50

var floating_text = preload("res://UI/floating_text.tscn")

var player_detected := false
var player_reference : Player
var trigger_attack := false
var cancel_attack := false
var is_dying := false

func knockback(direction, knockback_power):
	velocity = velocity / 2
	velocity += direction * knockback_power

func flash():
	var flash_tween = get_tree().create_tween()
	flash_tween.tween_property($Sprite2D, "modulate", Color(5, 5, 5, 1), 0.1)
	flash_tween.tween_property($Sprite2D, "modulate", Color(1, 1, 1, 1), 0.1)
