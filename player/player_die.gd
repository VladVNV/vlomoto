extends PlayerState

func enter(_msg := {}) -> void:
	death_anim()

func physics_update(delta) -> void:
	player.velocity = lerp(player.velocity, Vector2.ZERO, player.friction / 2.0 * delta)

func death_anim() -> void:
	player.animation_player.play("death")
	
	# TODO: make the game over screen appear
