extends CharacterBody2D
class_name Player

@onready var char_body = $CharBody
@onready var back_arm = $BackArm
@onready var front_arm = $FrontArm
@onready var animation_player = $AnimationPlayer

@export_category("movement")
@export var speed := 120
@export var acceleration := 20
@export var friction := 13

@export_category("dodge")
@export var dodge_time := 0.12
@export var dodge_velocity := 200
@export var dodge_cooldown := 0.5
@onready var dodge_timer = $DodgeTimer

@export_category("stats")
@export var max_health := 100
var health : int

var floating_text = preload("res://UI/floating_text.tscn")

# possibly handle weapon differently? is it better not to instantiate it under the pivot?
@onready var weapon_pivot = $WeaponPivot
@onready var damage_text_marker = $DamageTextMarker

var ammo = { 0: -1 }
var active_ammo = 0
const MOZARELLA_GUN = preload("res://weapons/mozarella_gun.tscn")
const SWISS_GUN = preload("res://weapons/swiss_gun.tscn")
const NACHO_GUN = preload("res://weapons/nacho_gun.tscn")
const AMERICAN_GUN = preload("res://weapons/american_gun.tscn")
const PIZZA_GUN = preload("res://weapons/pizza_gun.tscn")

var pizza_parts = { 0: false, 1: false, 2: false }

# AUDIO
@onready var announcement = $Announcement
@onready var style_sfx = $StyleSFX
const AMERICAN = preload("res://assets/audio/sfx/momohut/american.wav")
const EAGLE = preload("res://assets/audio/sfx/momohut/eagle.wav")
const NACHO = preload("res://assets/audio/sfx/momohut/nacho.wav")
const SWISS = preload("res://assets/audio/sfx/momohut/swiss.wav")
const YODEL = preload("res://assets/audio/sfx/momohut/yodel.wav")

@onready var run_sfx = $RunSFX
@onready var dodge_sfx = $DodgeSFX

var is_dying := false

func _ready():
	health = max_health

func get_input_direction() -> Vector2:
	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var direction_x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	var direction_y = Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
	var direction = Vector2(direction_x, direction_y).normalized()
	
	# TODO: Possibly need to flip sprite direction here via flip_h
	if direction.is_zero_approx():
		char_body.stop()
	else:
		char_body.play()
	
	return direction

func _input(event):
	if event.is_action_pressed("quit"):
		get_tree().quit()
	elif event.is_action_pressed("next_weapon"):
		next_weapon()
	elif event.is_action_pressed("previous_weapon"):
		previous_weapon()

func can_dodge() -> bool:
	return dodge_timer.is_stopped()

func count_dodge():
	dodge_timer.set_wait_time(dodge_cooldown)
	dodge_timer.start()

func can_attack() -> bool:
	if ammo[active_ammo] == -1 or ammo[active_ammo] > 0:
		return weapon_pivot.get_child(0).can_attack()
	else:
		return false

func attack():
	weapon_pivot.get_child(0).attack()

func pivot_weapon():
	weapon_pivot.look_at(get_global_mouse_position())
	front_arm.look_at(get_global_mouse_position())
	back_arm.look_at(get_global_mouse_position())
	
	flip_h()

func flip_h():
	if (get_global_mouse_position() - global_position < Vector2.ZERO) and not char_body.is_flipped_h():
		char_body.set_flip_h(true)
		weapon_pivot.get_child(0).flip_h()
		back_arm.set_flip_v(true)
		front_arm.set_flip_v(true)
	if (get_global_mouse_position() - global_position >= Vector2.ZERO) and char_body.is_flipped_h():
		char_body.set_flip_h(false)
		weapon_pivot.get_child(0).flip_h()
		back_arm.set_flip_v(false)
		front_arm.set_flip_v(false)

func add_rune(rune_input):
	weapon_pivot.get_child(0).add_rune(rune_input)
	
	# this should eventually be changed
	match rune_input:
		"fire":
			weapon_pivot.get_child(0).set_modulate(Color(2.0, 0.2, 0.2, 1.0))
		"water":
			weapon_pivot.get_child(0).set_modulate(Color(0.2, 0.2, 2.0, 1.0))
		_:
			weapon_pivot.get_child(0).set_modulate(Color(2.0, 2.0, 2.0, 1.0))

func flash():
	var flash_tween = get_tree().create_tween()
	flash_tween.tween_property($CharBody, "modulate", Color(5, 5, 5, 1), 0.1)
	flash_tween.tween_property($CharBody, "modulate", Color(1, 1, 1, 1), 0.1)

func hit(source):
	# pass damage source to get info like damage value and type
	health -= source.damage
	if health <= 0:
		is_dying = true
	print_damage(source.damage, source.type)
	knockback(source.direction, source.knockback_power)
	flash()
	
	for child in get_tree().current_scene.get_children():
		if child.has_method("set_health_shader"):
			child.set_health_shader(health)

func print_damage(damage, type):
	# generate damage text
	var text = floating_text.instantiate()
	text.value = damage
	text.type = type
	text.set_position(damage_text_marker.position)
	add_child(text)

func knockback(direction, knockback_power):
	velocity = velocity / 2
	velocity += direction * knockback_power

func add_ammo(type : GlobalConstants.CHEESE_TYPE, quantity : int):
	var ammo_present = ammo.get(type)
	if ammo_present:
		ammo[type] += quantity
	else:
		ammo[type] = quantity
		active_ammo = ammo.keys()[ammo.size() - 1]
		swap_weapon()
		pickup_sfx()
	print("ammo dict = " + str(ammo))
	#print("ammo index = " + str(active_ammo))

func next_weapon():
	if ammo.size() > 1:
		if active_ammo == ammo.keys()[ammo.size() - 1]:
			active_ammo = ammo.keys()[0]
		else:
			var keys = ammo.keys()
			var index = keys.find(active_ammo)
			active_ammo = ammo.keys()[index + 1]
		
		print("active_ammo = " + str(GlobalConstants.CHEESE_TYPE.keys()[active_ammo]))
		swap_weapon()

func previous_weapon():
	if ammo.size() > 1:
		if active_ammo == ammo.keys()[0]:
			active_ammo = ammo.keys()[ammo.size() - 1]
		else:
			var keys = ammo.keys()
			var index = keys.find(active_ammo)
			active_ammo = ammo.keys()[index - 1]
		
		print("active_ammo = " + str(GlobalConstants.CHEESE_TYPE.keys()[active_ammo]))
		swap_weapon()

func swap_weapon():
	weapon_pivot.get_child(0).queue_free()
	match active_ammo:
		GlobalConstants.CHEESE_TYPE.mozarella:
			var weapon = MOZARELLA_GUN.instantiate()
			weapon_pivot.add_child(weapon)
		GlobalConstants.CHEESE_TYPE.swiss:
			var weapon = SWISS_GUN.instantiate()
			weapon_pivot.add_child(weapon)
		GlobalConstants.CHEESE_TYPE.nacho:
			var weapon = NACHO_GUN.instantiate()
			weapon_pivot.add_child(weapon)
		GlobalConstants.CHEESE_TYPE.american:
			var weapon = AMERICAN_GUN.instantiate()
			weapon_pivot.add_child(weapon)
		GlobalConstants.CHEESE_TYPE.pizza:
			var weapon = PIZZA_GUN.instantiate()
			weapon_pivot.add_child(weapon)
	
	for child in get_tree().current_scene.get_children():
		if child.has_method("set_ammo_texture"):
			child.set_ammo_texture(active_ammo)
		if child.has_method("set_ammo_value"):
			child.set_ammo_value(ammo[active_ammo], active_ammo)

func use_ammo(amount: int):
	ammo[active_ammo] -= amount
	for child in get_tree().current_scene.get_children():
		if child.has_method("set_ammo_value"):
			child.set_ammo_value(ammo[active_ammo], active_ammo)

func pickup_sfx():
	match active_ammo:
		GlobalConstants.CHEESE_TYPE.swiss:
			announcement.set_stream(SWISS)
			style_sfx.set_stream(YODEL)
			announcement.play()
			style_sfx.play()
		GlobalConstants.CHEESE_TYPE.nacho:
			announcement.set_stream(NACHO)
			#style_sfx.set_stream(YODEL)
			announcement.play()
			#style_sfx.play()
		GlobalConstants.CHEESE_TYPE.american:
			announcement.set_stream(AMERICAN)
			style_sfx.set_stream(EAGLE)
			announcement.play()
			style_sfx.play()
		GlobalConstants.CHEESE_TYPE.pizza:
			pass

func add_ingredient(pizza_ingredient : int):
	pizza_parts[pizza_ingredient] = true
	
	for ingredient in pizza_parts.values():
		if not ingredient:
			return
	
	spawn_pizza_gun()

func spawn_pizza_gun():
	ammo.erase(GlobalConstants.CHEESE_TYPE.mozarella)
	ammo[GlobalConstants.CHEESE_TYPE.pizza] = -1
	
	#active_ammo = ammo.keys()[GlobalConstants.CHEESE_TYPE.pizza]
	active_ammo = ammo.keys()[ammo.size() - 1]
	swap_weapon()

func player_position():
	return global_position
