extends Control

@onready var label = $MarginContainer/Label

var cheese_type : GlobalConstants.CHEESE_TYPE

# Called when the node enters the scene tree for the first time.
func _ready():
	label.set_text(get_text())
	
	fade_out()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func get_text():
	match cheese_type:
		GlobalConstants.CHEESE_TYPE.swiss:
			return "Swiss"
		GlobalConstants.CHEESE_TYPE.nacho:
			return "Nacho"
		GlobalConstants.CHEESE_TYPE.american:
			return "American"

func fade_out():
	var scale_tween = get_tree().create_tween()
	scale_tween.tween_property(self, "scale", Vector2(3.0, 3.0), 1.0)
	var mod_tween = get_tree().create_tween()
	mod_tween.tween_property(self, "modulate", Color(1.0, 1.0, 1.0, 0.0), 1.0)
	
	await mod_tween.finished
	queue_free()
