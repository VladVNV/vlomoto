extends CanvasLayer

@onready var cheese_texture = $MarginContainer/VBoxContainer/MarginContainer/HBoxContainerRight/CheeseContainer/TextureRect
@onready var infinity_container = $MarginContainer/VBoxContainer/MarginContainer/HBoxContainerRight/InfinityContainer
@onready var number_container = $MarginContainer/VBoxContainer/MarginContainer/HBoxContainerRight/NumberContainer
@onready var label = $MarginContainer/VBoxContainer/MarginContainer/HBoxContainerRight/NumberContainer/Label
@onready var texture_rect = $MarginContainer/VBoxContainer/MarginContainer/HBoxContainerLeft/HeartsContainer/MarginContainer/TextureRect


const MOZARELLA = preload("res://assets/objects/mozarella.png")
const SWISS = preload("res://assets/objects/swiss.png")
const NACHO = preload("res://assets/objects/nacho.png")
const AMERICAN = preload("res://assets/objects/american.png")
const PIZZA = preload("res://assets/objects/pizza.png")

func set_ammo_value(value : int, cheese_type):
	if cheese_type == GlobalConstants.CHEESE_TYPE.mozarella or \
	cheese_type == GlobalConstants.CHEESE_TYPE.pizza:
		infinity_container.show()
		number_container.hide()
	else:
		infinity_container.hide()
		number_container.show()
		label.set_text(str(value))

func set_ammo_texture(cheese_type):
	match cheese_type:
		GlobalConstants.CHEESE_TYPE.mozarella:
			cheese_texture.set_texture(MOZARELLA)
		GlobalConstants.CHEESE_TYPE.swiss:
			cheese_texture.set_texture(SWISS)
		GlobalConstants.CHEESE_TYPE.nacho:
			cheese_texture.set_texture(NACHO)
		GlobalConstants.CHEESE_TYPE.american:
			cheese_texture.set_texture(AMERICAN)
		GlobalConstants.CHEESE_TYPE.pizza:
			cheese_texture.set_texture(PIZZA)

func set_health_shader(value : int):
	var material = texture_rect.get_material()
	material.set_shader_parameter("health_percent", value / 100.0)
