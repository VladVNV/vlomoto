Potential assets:


Runes:
	https://kenney.nl/assets/rune-pack


UI:
	borders:
		https://kenney.nl/assets/fantasy-ui-borders
	icons:
		https://kenney.nl/assets/board-game-icons
		https://kenney.nl/assets/input-prompts-pixel-16
	pixel:
		https://kenney.nl/assets/pixel-ui-pack
	rpg style UI:
		https://kenney.nl/assets/ui-pack-rpg-expansion


Tilemap:
	https://kenney.nl/assets/roguelike-rpg-pack


Characters:
	https://kenney.nl/assets/roguelike-characters
		- only front facing
		- very customizable
	https://kenney.nl/assets/toon-characters-1
		- 45 poses per character
	https://kenney.nl/assets/shape-characters
		- potentially cute/funny characters
	https://kenney.nl/assets/sokoban
		- simple character in 4 cardinal directions


Weapons:
	https://kenney.nl/assets/tiny-dungeon
		- characters, weapons, tiles
	https://kenney.nl/assets/tiny-town
		- simple weapons and tiles
	https://kenney.nl/assets/platformer-art-requests
		- ray guns, swords and shields


Particles:
	https://kenney.nl/assets/particle-pack


Fonts:
	https://kenney.nl/assets/kenney-fonts
