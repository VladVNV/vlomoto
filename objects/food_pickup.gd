extends Area2D

@export var pizza_count : int

@onready var sprite_2d = $Sprite2D

const PIZZA_TOMATO = preload("res://assets/objects/pizza_tomato.png")
const PIZZA_DOUGH = preload("res://assets/objects/pizza_dough.png")
const PIZZA_BASIL = preload("res://assets/objects/pizza_basil.png")

func _ready():
	match pizza_count:
		0:
			sprite_2d.set_texture(PIZZA_DOUGH)
		1:
			sprite_2d.set_texture(PIZZA_TOMATO)
		2:
			sprite_2d.set_texture(PIZZA_BASIL)

func _on_body_entered(body):
	if body.is_in_group("player"):
		body.add_ingredient(pizza_count)
	
	queue_free()
