extends Area2D

@export var type : GlobalConstants.CHEESE_TYPE
@export var quantity : int

@onready var sprite_2d = $Sprite2D

const MOZARELLA = preload("res://assets/objects/mozarella.png")
const SWISS = preload("res://assets/objects/swiss.png")
const NACHO = preload("res://assets/objects/nacho.png")
const CAMEMBERT = preload("res://assets/objects/camembert.png")
const AMERICAN = preload("res://assets/objects/american.png")

const NOTIFICATION = preload("res://UI/notification.tscn")

func _ready():
	match type:
		GlobalConstants.CHEESE_TYPE.mozarella:
			sprite_2d.set_texture(MOZARELLA)
		GlobalConstants.CHEESE_TYPE.swiss:
			sprite_2d.set_texture(SWISS)
		GlobalConstants.CHEESE_TYPE.nacho:
			sprite_2d.set_texture(NACHO)
		GlobalConstants.CHEESE_TYPE.camembert:
			sprite_2d.set_texture(CAMEMBERT)
		GlobalConstants.CHEESE_TYPE.american:
			sprite_2d.set_texture(AMERICAN)

func _on_body_entered(body):
	if body.is_in_group("player"):
		body.add_ammo(type, quantity)
		var notification = NOTIFICATION.instantiate()
		notification.cheese_type = type
		notification.global_position = global_position
		get_tree().current_scene.add_child(notification)
	
	queue_free()
